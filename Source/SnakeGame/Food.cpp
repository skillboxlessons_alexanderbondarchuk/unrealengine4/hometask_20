// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* interactor, bool isHead)
{
	if (isHead) {
		auto snake = Cast<ASnakeBase>(interactor);

		if (IsValid(snake)) {
			snake->AddSnakeElements();

			Spawn();
			Destroy();
		}
	}
}

void AFood::Spawn()
{
	const auto min = -450;
	const auto max = 450;
	const auto z = -63.f;
	const auto randNum = static_cast<float>(rand() % (max - min + 1) + min);

	const FVector location { randNum, randNum, z };
	FTransform transform{ location };

	GetWorld()->SpawnActor<AFood>(FoodElementClass, transform);
}