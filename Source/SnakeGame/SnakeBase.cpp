// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::Down;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorTickInterval(MovementSpeed);
	AddSnakeElements(3);
}

// Called every frame
void ASnakeBase::Tick(float deltaTime)
{
	Super::Tick(deltaTime);
	Move();
}

void ASnakeBase::AddSnakeElements(int amount) {
	for (int i = 0; i < amount; ++i) {
		const auto snakeElementsAmount = SnakeElements.Num();

		FVector newLocation{ snakeElementsAmount * ElementSize, 0, 0 };
		const auto lastElementIndex = snakeElementsAmount - 1;

		if (snakeElementsAmount > 1)
		{
			newLocation = SnakeElements[lastElementIndex]->GetActorLocation();
		}

		FTransform newTransform{ newLocation };
		ASnakeElementBase* newSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, newTransform);
		
		newSnakeElement->SnakeOwner = this;
		const auto elemIndex = SnakeElements.Add(newSnakeElement);

		if (elemIndex == 0) {
			newSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector movementVector { ForceInitToZero };

	switch (LastMoveDirection) {
	case EMovementDirection::Up :
		movementVector.X += ElementSize;
		break;
	case EMovementDirection::Down:
		movementVector.X -= ElementSize;
		break;
	case EMovementDirection::Right:
		movementVector.Y += ElementSize;
		break;
	case EMovementDirection::Left:
		movementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();//bond выглядит как костыль

	//AddActorWorldOffset(movementVector);
	for (auto i = SnakeElements.Num()-1; i > 0; i--)
	{
		auto currentElement = SnakeElements[i];
		const auto prevElement = SnakeElements[i-1];

		auto prevLocation = prevElement->GetActorLocation();
		currentElement->SetActorLocation(prevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(movementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* overlappedElement, AActor* other)
{
	if (IsValid(overlappedElement)) {
		int32 elemIndex;
		SnakeElements.Find(overlappedElement, elemIndex);

		const bool isHead = elemIndex == 0;

		IInteractable* obj = Cast<IInteractable>(other);
		
		if (obj) {
			obj->Interact(this, isHead);
		}
	}
}